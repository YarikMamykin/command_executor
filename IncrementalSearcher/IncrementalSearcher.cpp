#include <IncrementalSearcher.h>

IncrementalSearcher::IncrementalSearcher(const std::set<std::string>& db) : db(db) {}

void IncrementalSearcher::push(char c) { 

	if(c == 0) return;

	using StepResults = std::set<std::string_view>;

	StepResults results;
	if(steps.empty()) {

		for(const auto& filename: db) {
			if(filename.size() > steps.size()) {
				if(filename[steps.size()] == c) {
					results.insert(filename);
				}
			}
		}

	} else {

		for(const auto& filename: steps.back().second) {
			if(filename.size() > steps.size()) {
				if(filename[steps.size()] == c) {
					results.insert(filename);
				}
			}
		}

	}

	steps.emplace_back(c, results);
	selection_index = std::distance(steps.back().second.begin(), steps.back().second.begin());
}

void IncrementalSearcher::pop() {
	if(steps.empty()) {
		return;
	}

	steps.erase(std::prev(steps.end()));
	selection_index = std::distance(steps.back().second.begin(), steps.back().second.begin());
}

void IncrementalSearcher::erase() {
	steps.clear();
}

std::string IncrementalSearcher::search_string() const {
	char result[steps.size()];
	std::size_t index {0ul};
	for(const auto& [c, _]: steps) { 
		result[index++] = c;
	}
	return std::string(result, result+steps.size());
}

std::set<std::string_view>::difference_type IncrementalSearcher::selected_match_index() const {
	return selection_index;
}

std::string_view IncrementalSearcher::selected_match() const {
	auto last_step_results = steps.back().second.begin();
	return *std::next(last_step_results, selection_index);
}

bool IncrementalSearcher::next_selection() {
	if(steps.empty()) 
		return false; 

	if(selection_index < steps.back().second.size() - 1) {
		++selection_index;
		return true; 
	}

	return false; 
}

bool IncrementalSearcher::prev_selection() {
	if(steps.empty()) 
		return false; 

	if(selection_index != 0) {
		--selection_index;
		return true; 
	}

	return false;
}

std::string_view IncrementalSearcher::longest_match() const {
	return *std::max_element(steps.back().second.begin(), steps.back().second.end(), [](const auto& current, const auto& next) { return current.size() < next.size();});
}
