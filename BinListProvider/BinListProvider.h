#pragma once

#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <string_view>
#include <utility>
#include <vector>
#include <list>
#include <set>
#include <filesystem>
#include <unordered_map>
#include <algorithm>


struct BinListProvider {

	BinListProvider(std::vector<std::string>&& path_list);

	void print_binaries() const;

	std::string_view longest_binary_name() const;

	private:
	bool is_file_executable(const std::string& path) const;

	public:
		std::unordered_map<std::string, std::set<std::string>> binaries; 
};

