#include <CommandExecutor.h>

int main() {
	auto command_executor = std::make_unique<CommandExecutor>();
	command_executor->run();
	return 0;
}
