#pragma once

#include <string>
#include <vector>

struct BinExecutioner {

	int execute(const std::string& bin_path, std::vector<std::string>&& args) const;

};
