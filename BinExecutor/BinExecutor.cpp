#include <BinExecutor.h>
#include <sstream>
#include <thread>

int BinExecutioner::execute(const std::string& bin_path, std::vector<std::string>&& args) const {

	std::thread([bin_path = std::move(bin_path), args = std::move(args)]() mutable -> void {

		std::stringstream ss;

		for(const auto& arg: args) {
			ss << " " << arg;
		}

		std::string cmd = bin_path + ss.str();

		if(0 != std::system(cmd.c_str())) {
			// maybe put some error log...
		}

	}).detach();

	return 0;
}
