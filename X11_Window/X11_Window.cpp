#include <X11_Window.h>

X11_Window::X11_Window(XlibHandlers& xhandlers) 
: xhandlers(xhandlers) 
, input_pos(XPoint{.x = 10, .y = 20}) {

	auto& [ d, s, gc, font_info, font_height, w ] = xhandlers;
	d = XOpenDisplay(nullptr);
	s = XDefaultScreen(d);
	gc = XDefaultGC(d, s);
	font_info = XLoadQueryFont(d, "*-14-*");
	XSetFont(d, gc, font_info->fid);
	font_height = font_info->ascent + font_info->descent;

	constexpr unsigned long bgnd = 0x222222ul;
	w = XCreateSimpleWindow(
			d, RootWindow(d, s), 
			0,0,XDisplayWidth(d, s), font_height + 10, 1, 0ul, bgnd);

	XSelectInput(d, w, ExposureMask | KeyPressMask );
	XStoreName(d,w,"command_executor");
	XMapWindow(d, w);
	XRaiseWindow(d, w);
	XSync(d, s);
}

X11_Window::~X11_Window() {
	auto& [ d, s, gc, font_info, font_height, w ] = xhandlers;
	XUnmapWindow(d, w);
	XDestroyWindow(d, w);
	XFreeFont(d, font_info);
	XCloseDisplay(d);
}

int X11_Window::graphical_width(const std::string& s) {
	return graphical_width(std::string_view{ s });
}

int X11_Window::graphical_width(std::string_view s) {
	return XTextWidth(xhandlers.font_info, s.data(), s.size());
}

void X11_Window::draw_label(LabelInfo&& info) {

	auto& [ pos, text, colormap, option ] = info;
	auto& [ x, y ] = pos;
	auto& [ tc, fc ] = colormap;
	auto& [ d, s, gc, font_info, font_height, w ] = xhandlers;

	XSetForeground(d, gc, tc);
	XDrawString(d, w, gc, x, y, text.data(), text.size());

	if(option == LABEL_OPTION::WITH_FRAME) {
		XSetForeground(d, gc, fc);
		XDrawRectangle(d, w, gc, x - 5, y - font_height + font_info->descent - 5, graphical_width(text) + 10, font_height + 10);
	}
}

void X11_Window::draw_start_message(const std::string& start_message) {
	draw_label(LabelInfo { 
			input_pos, 
			start_message, 
			{ .text = 255ul << 8 }, 
			LABEL_OPTION::NO_FRAME
	});
}

void X11_Window::draw_matches(const std::set<std::string_view>& matches) {
	current_matches = &matches;
	draw_matches_from_selected(0);
}

void X11_Window::draw_matches_from_selected(std::set<std::string_view>::difference_type match_index) {

	if(current_matches->empty()) return;

	auto& [ d, s, gc, font_info, font_height, w ] = xhandlers;

	const int display_width = XDisplayWidth(d, s);

	auto match_it = current_matches->begin();
	std::advance(match_it, match_index);

	XPoint pos = input_pos;

	for(; match_it != current_matches->end(); ++match_it) {

		draw_label(LabelInfo{
				{ pos.x, pos.y },
				*match_it,
				{ .text = 255ul << 16 },
				LABEL_OPTION::NO_FRAME
		});

		pos.x = pos.x + 10 + graphical_width(*match_it);

		if(pos.x > display_width)
			break;
	}
}

void X11_Window::draw_selection(std::set<std::string_view>::difference_type match_index) {

	if(current_matches->empty()) return;

	clear();
	draw_matches_from_selected(match_index);

	auto match_it = current_matches->begin();
	std::advance(match_it, match_index);

	draw_label(LabelInfo { 
			{.x = input_pos.x, .y = input_pos.y}, 
			*match_it, 
			{ .text = 255ul, .frame = ~0ul }, 
			LABEL_OPTION::WITH_FRAME 
	});
}

void X11_Window::draw_user_input(const std::string& input_string) {

	if(input_string.empty()) return;

	auto& [ d, s, gc, font_info, font_height, w ] = xhandlers;

	draw_label(LabelInfo { 
			input_pos, 
			input_string, 
			{ .text = 255ul << 8 }, 
			LABEL_OPTION::NO_FRAME
	});
}

void X11_Window::clear() {
	auto& [ d, s, gc, font_info, font_height, w ] = xhandlers;
	XClearWindow(d, w);
	XSync(d, s);
}

void X11_Window::set_size(unsigned int width, unsigned int height) {
	auto& [ d, s, gc, font_info, font_height, w ] = xhandlers;
	XResizeWindow(d, w, width, height);
	XSync(d, s);
}
