#pragma once

#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <string_view>
#include <tuple>
#include <utility>
#include <vector>
#include <list>
#include <set>
#include <filesystem>
#include <unordered_map>
#include <algorithm>

#include <XlibHandlers.h>

struct X11_Window {

	X11_Window(XlibHandlers& xhandlers);

	~X11_Window();

	int graphical_width(const std::string& s);

	int graphical_width(std::string_view s);

	enum class LABEL_OPTION {
		NO_FRAME,
		WITH_FRAME
	};
	
	// return graphical_width of label
	struct LabelColorMap { unsigned long text, frame; };
	using LabelInfo = std::tuple<XPoint, std::string_view, LabelColorMap, LABEL_OPTION>;
	void draw_label(LabelInfo&& info);

	void draw_start_message(const std::string& start_message);

	void draw_matches(const std::set<std::string_view>& matches);

	void draw_matches_from_selected(std::set<std::string_view>::difference_type match_index);

	void draw_selection(std::set<std::string_view>::difference_type match_index);

	void draw_user_input(const std::string& input_string);

	void clear();

	void set_size(unsigned int width, unsigned int height);

	XlibHandlers& xhandlers;
	XPoint input_pos;

	const std::set<std::string_view>* current_matches;
};
