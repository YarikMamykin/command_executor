#pragma once

#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <string_view>
#include <utility>
#include <vector>
#include <list>
#include <set>
#include <filesystem>
#include <unordered_map>
#include <algorithm>

#include <XlibHandlers.h>

enum class KEY_MASK : unsigned int {
	NONE = 0,
	CTRL = ControlMask,
	SHIFT = ShiftMask
};

struct InputHandler {

	InputHandler(XlibHandlers& xhandlers);

	~InputHandler();

	using InputData = std::tuple<int, KEY_MASK, char>;
	InputData read_input();

	private:
		char get_input_char(XEvent& e);

		XlibHandlers& xhandlers;
};

